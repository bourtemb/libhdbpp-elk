# Libhdbpp-ElasticSearch

[HDB++](http://www.tango-controls.org/community/projects/hdbplus) library for Apache ElasticSearch backend. This library is loaded by [Libhdbpp](https://github.com/tango-controls-hdbpp/libhdbpp) to archive events from a Tango Controls system.

## Version

The current release version is 0.1.0.

## Documentation

* See the tango documentation [here](http://tango-controls.readthedocs.io/en/latest/administration/services/hdbpp/index.html#hdb-an-archiving-historian-service) for broader information about the HB++ archiving system and its integration into Tango Controls
* Libhdbpp-ElasticSearch [CHANGELOG.md](CHANGELOG.md) contains the latest changes both released and in development.

## Bugs Reports

Please file bug reports above in the issues section.

## Building and Installation

See the [INSTALL.md](INSTALL.md) file for  detailed instructions on how to build and install libhdbpp-ElasticSearch.

## License

The source code is released under the LGPL3 license and a copy of this license is provided with the code.